from django.conf.urls import url
from drop.views import home

urlpatterns = [
    url(r'^$', home, name='home'),
    #url(r'^(?P<alias>[^/]+)$', droper, name='droper'),
]