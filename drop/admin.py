from django.contrib import admin
from drop.models import Item


class ItemAdmin(admin.ModelAdmin):
    list_display = ('code', 'title', 'buyprice', 'dropprepay', 'dropaftpay', 'saleprice')
    list_display_links = ('code', 'title')
    list_per_page = 25
    prepopulated_fields = {"alias": ('code',)}


admin.site.register(Item, ItemAdmin)
