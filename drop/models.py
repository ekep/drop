from django.db import models

class Item(models.Model):
    title = models.CharField(max_length=255, verbose_name='Назва товара')
    code = models.CharField(max_length=255, verbose_name='Код товара')
    alias = models.SlugField(verbose_name='Alias товара')
    buyprice = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Ціна закупки')
    dropprepay = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Ціна дропшпредоплата')
    dropaftpay = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Ціна дроппісляоплата')
    saleprice = models.DecimalField(max_digits=8, decimal_places=2, verbose_name='Ціна продажі')
    quantity = models.IntegerField(verbose_name='Кількість')
    photo = models.ImageField(upload_to='static/item/', verbose_name='Фото товара')
    text = models.TextField(verbose_name='Опис товара')

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товари"

    def __str__(self):
        return self.title
