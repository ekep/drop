from django.shortcuts import render_to_response
from drop.models import Item


def home(request):
    args = {}
    args['title'] = 'Головна'
    args['countitem'] = Item.objects.count()
    args['items'] = Item.objects.all()
    return render_to_response('home.html', args)
